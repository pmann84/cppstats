#ifndef __VALIDATION_UTILS_H__
#define __VALIDATION_UTILS_H__
#include <vector>

namespace stats
{
   namespace validationutils
   {
      template<class T>
      bool verify_sizes_are_equal(const std::vector<T>& vector1, const std::vector<T>& vector2)
      {
         return vector1.size() == vector2.size();
      }

      template<class T>
      bool verify_empty(const std::vector<T>& vector)
      {
         return vector.empty();
      }

      template<class T>
      bool verify_empty(typename std::vector<T>::const_iterator& begin, typename std::vector<T>::const_iterator& end)
      {
         return std::distance(begin, end) == 0;
      }
   }
}

#endif // __VALIDATION_UTILS_H__
