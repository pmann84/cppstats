#ifndef __STATS_UTILS_H__
#define __STATS_UTILS_H__
#include <cmath>

namespace stats
{
   namespace statsutils
   {
      inline double factorial(int n)
      {
         // TODO: Need to examine data types here - double may not be big enough
         return tgamma(n + 1);
      }

      inline double binomial_coefficient(int n, int k)
      {
         // (n, k) = n! / k! (n-k)!
         // n is the nth row of pascals triangle, k is the kth element
         return factorial(n) / (factorial(k) * factorial(n - k));
      }
   }
}

#endif // __STATS_UTILS_H__