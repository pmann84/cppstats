#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../cppstats/statsutils.h"

// TODO: Test average_absolute_deviation.
// TODO: Test pearson correlation.
// TODO: Test covariance

TEST(TestStatsUtils, TestFactorialBasic)
{
   EXPECT_THAT(1, stats::statsutils::factorial(0));
   EXPECT_THAT(1, stats::statsutils::factorial(1));
   EXPECT_THAT(2, stats::statsutils::factorial(2));
   EXPECT_THAT(6, stats::statsutils::factorial(3));
   EXPECT_THAT(24, stats::statsutils::factorial(4));
   EXPECT_THAT(120, stats::statsutils::factorial(5));
}

TEST(TestStatsUtils, TestBinomialCoefficients)
{
   EXPECT_THAT(stats::statsutils::binomial_coefficient(0, 0), 1);

   EXPECT_THAT(stats::statsutils::binomial_coefficient(1, 0), 1);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(1, 1), 1);
   
   EXPECT_THAT(stats::statsutils::binomial_coefficient(2, 0), 1);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(2, 1), 2);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(2, 2), 1);

   EXPECT_THAT(stats::statsutils::binomial_coefficient(3, 0), 1);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(3, 1), 3);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(3, 2), 3);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(3, 3), 1);

   EXPECT_THAT(stats::statsutils::binomial_coefficient(4, 0), 1);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(4, 1), 4);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(4, 2), 6);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(4, 3), 4);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(4, 4), 1);

   EXPECT_THAT(stats::statsutils::binomial_coefficient(5, 0), 1);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(5, 1), 5);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(5, 2), 10);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(5, 3), 10);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(5, 4), 5);
   EXPECT_THAT(stats::statsutils::binomial_coefficient(5, 5), 1);
}