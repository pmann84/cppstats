#ifndef __STATS_H__
#define __STATS_H__

#include <vector>

#include "seriesutils.h"
#include "validationutils.h"

namespace stats
{
   /// Struct to hold parameters of a linear regression.
   /** This struct holds the parameters of a linear regression
    ** in one variable, more accurately, gradient and y 
    ** intercept */
   struct linregr
   {
      double gradient;
      double intercept;
   };

   /// Calculates the arithmetic mean of a series of data
   /** This calculates the aritmetic mean of 
    ** a series of data. Basically taking the sum
    ** of all the elements and dividing by the number
    ** of elements. */
   template<typename T>
   double mean(typename std::vector<T>::const_iterator& begin, typename std::vector<T>::const_iterator& end)
   {
      const unsigned int n = std::distance(begin, end);
      T sum = seriesutils::series_sum<T>(begin, end);
      return sum / static_cast<T>(n);
   };

   template<typename T>
   double mean(const std::vector<T>& data)
   {
      if (validationutils::verify_empty(data))
      {
         return 0;
      }
      return stats::mean<T>(data.cbegin(), data.cend());
   };

   /// Calculates the median of a series of data
   /** This calculates the aritmetic mean of
    ** a series of data. Basically taking the 
    ** element in the middle of the data set. If
    ** the data has an even number of data points
    ** it takes the average of the two middle points */
   template<typename T>
   double median(const std::vector<T>& data)
   {
      if (validationutils::verify_empty(data))
      {
         return 0;
      }
      const unsigned int n = data.size();
      std::vector<T> newdata(data);
      std::sort(newdata.begin(), newdata.end());
      if (newdata.size() % 2 == 0) // even
      {
         const int second_idx = int(std::ceil(newdata.size() / 2));
         const int first_idx = second_idx - 1;
         return (newdata[first_idx] + newdata[second_idx]) / 2;
      }
      else
      {
         return newdata[int(std::ceil(newdata.size() / 2))];
      }
   };

   /// Calculates the covariance between two variables
   /** Covariance is a measure of how changes in one variable 
    ** are associated with changes in a second variable. 
    ** Specifically, covariance measures the degree to which 
    ** two variables are linearly associated. */
   template<typename T>
   double covariance(const std::vector<T>& x, const std::vector<T>& y)
   {
      // (1/ n-1) sum_i=1^n (x_i - x_bar)(y_i - y_bar)
      if (!validationutils::verify_sizes_are_equal(x, y))
      {
         throw;
      }
      const unsigned int n_x = x.size();
      const unsigned int n_y = y.size();
      const double mu_x = stats::mean(x);
      const double mu_y = stats::mean(y);
      double covariance = 0;
      for (int i = 0; i < n_x; ++i)
      {
         covariance += (x[i] - mu_x)*(y[i] - mu_y);
      }
      return std::sqrt(covariance / (n_x - 1));
   }

   /// Calculates the variance of a series of data
   /** This calculates the variance of a series of 
    ** data. Informally, it measures how far a set 
    ** of (random) numbers are spread out from their 
    ** average value. Basically the squared deviation
   * of the elements. */
   template<typename T>
   double variance(typename std::vector<T>::const_iterator& begin, typename std::vector<T>::const_iterator& end)
   {
      // (1/ n-1) sum_i=1^n (x_i - x_bar)^2
      if (validationutils::verify_empty<T>(begin, end))
      {
         return 0;
      }

      T mean = stats::mean<T>(begin, end);
      std::function<T(T)> variance_functor = [&mean](T element) -> T { return std::pow(element - mean, 2); };
      T variance = seriesutils::series_sum(begin, end, variance_functor);
      const int n = std::distance(begin, end);
      return std::sqrt(variance / (n - 1));
   };

   template<typename T>
   double variance(const std::vector<T>& data)
   {
      return stats::variance<T>(data.cbegin(), data.cend());
   };

   /// Calculates the standard deviation of a series of data
   /** This calculates the standard deviation of a series 
    ** of data. It is a measure that is used to quantify 
    ** the amount of variation or dispersion of a set of 
    ** data values. A low standard deviation indicates that 
    ** the data points tend to be close to the mean (also 
    ** called the expected value) of the set, while a high 
    ** standard deviation indicates that the data points 
    ** are spread out over a wider range of values. */
   template<typename T>
   double standard_deviation(typename std::vector<T>::const_iterator& begin, typename std::vector<T>::const_iterator& end)
   {
      return std::sqrt(stats::variance<T>(begin, end));
   };

   template<typename T>
   double standard_deviation(const std::vector<T>& data)
   {
      return stats::standard_deviation<T>(data.cbegin(), data.cend());
   };

   /// Calculates the Average Absolute Deviation
   /** Calculates the deviation around a central midpoint. In reality this
    ** will be the mean, median or mode of the data. */
   template<typename T>
   double average_absolute_deviation(const std::vector<T>& data, const T midpoint)
   {
      if (validationutils::verify_empty(data))
      {
         return 0;
      }
      const unsigned int n = data.size();
      std::function<T(T)> series_function = [&midpoint](T series_element) -> T { return std::abs(series_element - midpoint); };
      T sum = seriesutils::series_sum(data, series_function);
      return sum / static_cast<T>(n);
   };

   /// Calculates the Relative Standard Deviation of a set of data
   /** This is a standardized measure of dispersion of a 
    ** probability distribution or frequency distribution. */
   template<typename T>
   double relative_standard_deviation(const std::vector<T>& data)
   {
      return std::sqrt(stats::variance(data)) / stats::mean(data);
   };

   /// Percentage increase
   template<typename T>
   double percentage_increase(T start, T end)
   {
      return (end - start) / static_cast<double>(start);
   }

   /// Calculates the Coefficient of Variation of a set of data
   /** This is a standardized measure of dispersion of a
    ** probability distribution or frequency distribution, 
    ** expressed as a percentage. */
   template<typename T>
   double coefficient_of_variation(const std::vector<T>& data)
   {
      return stats::relative_standard_deviation(data) * 100.0;
   };

   /// Calculates the quantiles given as a list of probabilities.
   /** Calculates the quantiles of the data for the given
    ** probabilities. These are cut points dividing the range 
    ** of a probability distribution into contiguous intervals 
    ** with equal probabilities. */
   template<typename T>
   std::vector<T> quantile(const std::vector<T>& data, const std::vector<T>& probs)
   {
      if (validationutils::verify_empty(data))
      {
         throw;
      }
      if (validationutils::verify_empty(probs))
      {
         throw;
      }
      // Sort the data
      std::vector<T> newdata(data);
      std::sort(newdata.begin(), newdata.end());
      // Now find quartiles
      std::vector<T> qus;
      for (auto prob : probs)
      {
         const double poi = (newdata.size() - 1)*prob;
         if (std::floor(poi) == poi)
         {
            qus.push_back(newdata[static_cast<int>(poi)]);
         }
         else
         {
            int right = std::ceil(poi);
            int left = right - 1;
            qus.push_back((newdata[left] + newdata[right]) / 2);
         }
      }
      return qus;
   }

   /// Computes a linear regression in one dimension
   /** Given a dependent variable, x and independent variable, 
    ** y, this calculates the linear regression for one dependent
    ** variable. This returns the gradient and intercept. */
   template<typename T>
   linregr linear_regression(const std::vector<T>& x, const std::vector<T>& y)
   {
      if (!validationutils::verify_sizes_are_equal(x, y))
      {
         throw;
      }
      const unsigned int n_x = x.size();
      linregr results;

      // Calculate x^2, y^2, xy
      std::vector<T> xx = seriesutils::series_square(x);
      std::vector<T> yy = seriesutils::series_square(y);
      std::vector<T> xy = seriesutils::series_product(x, y);
      // Assign values for ease
      const double s_x = seriesutils::series_sum(x);
      const double s_y = seriesutils::series_sum(y);
      const double s_xx = seriesutils::series_sum(xx);
      const double s_xy = seriesutils::series_sum(xy);

      const double linGradient = (n_x*s_xy - s_x*s_y) / (n_x*s_xx - std::pow(s_x, 2)); // n s(xy) - s(x)s(y) / n s(x2) - s(x)^2
      const double linIntercept = (s_y*s_xx - s_x*s_xy) / (n_x*s_xx - std::pow(s_x, 2));; // s(y)s(x2) - s(x)s(xy) / n s(x2) - s(x)^2
      // Fill out gradient results
      results.gradient = linGradient;
      results.intercept = linIntercept;
      return results;
   }

   /// Calculates the Pearson Product-Moment Correlation Coefficient
   /** This is a measure of linear correlation between two variables
    ** X and Y. It has a value between +1 and -1, where 1 is total
    ** positive linear correlation, 0 is no linear correlation, and
    ** -1 is total negative linear correlation. */
   template<typename T>
   double pearson_correlation(const std::vector<T>& x, const std::vector<T>& y)
   {
      if (!validationutils::verify_sizes_are_equal(x, y))
      {
         throw;
      }
      // Calculate standard deviations
      const double sigma_x = stddev(x);
      const double sigma_y = stddev(y);
      // Covariance of x and y
      const double cov_xy = covariance(x, y);
      return cov_xy / (sigma_x*sigma_y);
   }

   /// Calculates the Simple Moving average of a series of data
   /** This calculates the moving average. This returns a data set
    ** that has size n - (block_size - 1), where n is the size of the
    ** input data. Here the block size includes the element itself, so
    ** this will take the current data point plus the previous 
    ** (block_size - 1) elements and compute the average. */
   template<typename T>
   std::vector<T> simple_moving_average(const std::vector<T>& data, const int block_size)
   {
      // Validation
      if (validationutils::verify_empty(data))
      {
         throw;
      }

      // Compute the mean of the previous k-1 (blocksize) values
      std::vector<T> moving_average;
      int iterator_block_size = block_size - 1;
      // Calculate
      for (auto it = data.cbegin(); it != data.cend(); ++it)
      {
         auto dist_from_beginning = std::distance<typename std::vector<T>::const_iterator>(data.cbegin(), it);
         if (dist_from_beginning < iterator_block_size)
         {
            continue;
         }
         auto start_it = it - iterator_block_size;
         T mean = stats::mean<T>(start_it, it + 1);
         moving_average.push_back(mean);
      }
      return moving_average;
   }

   /// Calculates the Simple Moving average of a series of data centred on each data point
   /** This calculates the moving average. This returns a data set
   ** that has size n, where n is the size of the input data. The 
   ** average is calculated over the window of size block_size. 
   ** For block_size which is even the window is centred between 
   ** the current element and the previous. For odd windows it is 
   ** centred on the current element. */
   template<typename T>
   std::vector<T> simple_moving_average_centred(const std::vector<T>& data, const int block_size)
   {
      // Validation
      if (validationutils::verify_empty(data))
      {
         throw;
      }

      // Compute the mean of the previous k-1 (blocksize) values
      std::vector<T> moving_average;
      // Calculate - two cases
      // Even sized window - centred around current and previous entry
      // Odd sized entry - centred around current point
      int iterator_block_size = block_size % 2 == 0 ? block_size / 2 : (block_size - 1) / 2;
      for (auto it = data.cbegin(); it != data.cend(); ++it)
      {
         auto dist_from_beginning = std::distance<typename std::vector<T>::const_iterator>(data.cbegin(), it);
         auto dist_from_end = std::distance<typename std::vector<T>::const_iterator>(it, data.cend());

         auto start_it = dist_from_beginning < iterator_block_size ? data.cbegin() : it - iterator_block_size;
         
         auto end_it = dist_from_end <= iterator_block_size ? data.cend() : (block_size % 2 == 0 ? it + iterator_block_size : it + iterator_block_size + 1);

         T mean = stats::mean<T>(start_it, end_it);
         moving_average.push_back(mean);
      }
      return moving_average;
   }

   /// Calculates the Cumulative Moving average of a series of data centred on each data point
   /** This calculates the cumulative moving average. This is the 
    ** average of all values up to this point. */
   template<typename T>
   std::vector<T> cumulative_moving_average(const std::vector<T>& data)
   {
      // Validation
      if (validationutils::verify_empty(data))
      {
         throw;
      }

      // Compute the mean of the previous k-1 (blocksize) values
      std::vector<T> moving_average;
      // Calculate
      int count = 1;
      for (auto it = data.cbegin(); it != data.cend(); ++it)
      {
         if (it == data.cbegin())
         {
            moving_average.push_back(*it);
         }
         else
         {
            T cma_it = ((*it) + (count - 1)*moving_average.at(count - 2)) / (count - 1);
            moving_average.push_back(cma_it);
         }
         count++;
      }
      return moving_average;
   }

   /// Calculates the Simple Moving Standard Deviation of a series of data
   /** This calculates the moving stdev. This returns a data set
   ** that has size n - (block_size - 1), where n is the size of the
   ** input data. Here the block size includes the element itself, so
   ** this will take the current data point plus the previous
   ** (block_size - 1) elements and compute the stdev. */
   template<typename T>
   std::vector<T> simple_moving_standard_deviation(const std::vector<T>& data, const int block_size)
   {
      // Validation
      if (validationutils::verify_empty(data))
      {
         throw;
      }

      // Compute the mean of the previous k-1 (blocksize) values
      std::vector<T> moving_stdev;
      int iterator_block_size = block_size - 1;
      // Calculate
      for (auto it = data.cbegin(); it != data.cend(); ++it)
      {
         auto dist_from_beginning = std::distance<typename std::vector<T>::const_iterator>(data.cbegin(), it);
         if (dist_from_beginning < iterator_block_size)
         {
            continue;
         }
         auto start_it = it - iterator_block_size;

         T stddev = stats::standard_deviation<T>(start_it, it + 1);
         moving_stdev.push_back(std::sqrt(stddev));
      }
      return moving_stdev;
   }

   /// Calculates the centred Simple Moving average of a series of data centred on each data point
   /** This calculates the moving standard deviation. This returns 
    ** a data set that has size n, where n is the size of the input 
    ** data. The standard deviation is calculated over the window 
    ** of size block_size. For block_size which is even the window 
    ** is centred between the current element and the previous. For 
    ** odd windows it is centred on the current element. */
   template<typename T>
   std::vector<T> simple_moving_standard_deviation_centred(const std::vector<T>& data, const int block_size)
   {
      // Validation
      if (validationutils::verify_empty(data))
      {
         throw;
      }

      // Compute the mean of the previous k-1 (blocksize) values
      std::vector<T> moving_stddev;
      // Calculate - two cases
      // Even sized window - centred around current and previous entry
      // Odd sized entry - centred around current point
      int iterator_block_size = block_size % 2 == 0 ? block_size / 2 : (block_size - 1) / 2;
      for (auto it = data.cbegin(); it != data.cend(); ++it)
      {
         auto dist_from_beginning = std::distance<typename std::vector<T>::const_iterator>(data.cbegin(), it);
         auto dist_from_end = std::distance<typename std::vector<T>::const_iterator>(it, data.cend());

         auto start_it = dist_from_beginning < iterator_block_size ? data.cbegin() : it - iterator_block_size;
         auto end_it = dist_from_end <= iterator_block_size ? data.cend() : (block_size % 2 == 0 ? it + iterator_block_size : it + iterator_block_size + 1);

         T stddev = stats::standard_deviation<T>(start_it, end_it);
         moving_stddev.push_back(stddev);
      }
      return moving_stddev;
   }

   /// Calculates the Exponential Moving average of a series of data
   /** This calculates the exponetial moving average of a series 
    ** of data. This uses an alpha parameter of 2 / (N + 1) where
    ** N is the number of data points. */
   template<typename T>
   std::vector<T> exponential_moving_average(const std::vector<T>& data)
   {
      // Validation
      if (validationutils::verify_empty(data))
      {
         throw;
      }

      std::vector<T> ema;
      for (auto it = data.cbegin(); it != data.cend(); ++it)
      {
         const int num_data_points = std::distance(data.cbegin(), it);
         double alpha = 2.0 / (num_data_points + 1);
         if (it == data.cbegin())
         {
            // Initialise - just equal to first value
            ema.push_back(*it);
         }
         else
         {
            // General term -> alpha * data_{t} + (1-alpha) * ema_{t-1}
            ema.push_back(alpha*(*it) + (1.0 - alpha)*(ema.back()));
         }
      }
      return ema;
   }

   /// Calculates the Exponential Moving Standard Deviation of a series of data
   /** This calculates the exponetial deviation of a series
   ** of data. */
   template<typename T>
   std::vector<T> exponential_moving_standard_deviation(const std::vector<T>& data)
   {
      // Validation
      if (validationutils::verify_empty(data))
      {
         throw;
      }

      std::vector<T> ema;
      std::vector<T> emd;
      for (auto it = data.cbegin(); it != data.cend(); ++it)
      {
         const int num_data_points = std::distance(data.cbegin(), it);
         double alpha = 2.0 / (num_data_points + 1);
         if (it == data.cbegin())
         {
            // Initialise - just equal to first value
            ema.push_back(*it);
            emd.push_back(static_cast<T>(0));
         }
         else
         {
            // General term -> alpha * data_{t} + (1-alpha) * ema_{t-1}
            T delta = (*it) - ema.back();
            ema.push_back(ema.back() + alpha * delta);
            emd.push_back(std::sqrt((1.0 - alpha)*(std::pow(emd.back(), 2) + alpha * std::pow(delta, 2))));
         }
      }
      return emd;
   }
}

#endif // __STATS_H__