#ifndef __CSV_UTILS_H__
#define __CSV_UTILS_H__

#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <cctype>
#include <locale>
#include "validationutils.h"
#include <cstdlib>

static const std::string DELIMITER = ",";



inline void trim_whitespace_left(std::string &s)
{
   s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int c) { return !std::isspace(c); }));
}

inline void trim_whitespace_right(std::string &s)
{
   s.erase(std::find_if(s.rbegin(), s.rend(), [](int c) { return !std::isspace(c); }).base(), s.end());
}

inline void trim_whitespace(std::string &s)
{
   trim_whitespace_left(s);
   trim_whitespace_right(s);
}

inline std::vector<std::string> split_string(std::string txt, std::string delimiter)
{
   std::vector<std::string> strs;
   int pos = txt.find(delimiter);
   unsigned int initialPos = 0;
   strs.clear();

   // Decompose statement
   while (pos != std::string::npos) {
      std::string substrtxt = txt.substr(initialPos, pos - initialPos);
      trim_whitespace(substrtxt);
      strs.push_back(substrtxt);
      initialPos = pos + 1;

      pos = txt.find(delimiter, initialPos);
   }

   // Add the last one
   std::string substrtxt = txt.substr(initialPos, std::min(pos, txt.size()) - initialPos + 1);
   trim_whitespace(substrtxt);
   strs.push_back(substrtxt);
   return strs;
}

inline std::string join_string(std::vector<std::string> words, std::string delimiter)
{
   std::stringstream ss;
   for (auto word : words)
   {
      if (word == words.back())
      {
         ss << word;
      }
      else
      {
         ss << word << delimiter;
      }
   }
   return ss.str();
}

static void write(const std::string& filename, const std::vector<double>& x, const std::vector<double>& y)
{
   if (stats::validationutils::verify_empty(x) || 
       stats::validationutils::verify_empty(y) || 
      !stats::validationutils::verify_sizes_are_equal(x, y))
   {
      throw;
   }
   std::ofstream f;
   f.open(filename);
   for (int i = 0; i < x.size(); ++i)
   {
      f << x.at(i) << DELIMITER << y.at(i) << std::endl;
   }
   f.close();
}

static void read(const std::string& filename, const std::vector<double>& x, const std::vector<double>& y)
{
   std::ifstream f;
   f.open(filename);
   if (f.is_open())
   {
      std::string line;
      while (std::getline(f, line))
      {
         // Split line 
         std::vector<std::string> elements = split_string(line, DELIMITER);
         // first -> push back to x, second -> push back to y
         x.push_back(std::atof(elements[0].c_str()));
         y.push_back(std::atof(elements[1].c_str()));
      }
   }
   f.close();
}

#endif // __CSV_UTILS_H__
