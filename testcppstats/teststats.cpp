#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../cppstats/stats.h"

// TODO: Test average_absolute_deviation.
// TODO: Test pearson correlation.
// TODO: Test covariance

TEST(TestStats, TestAverageInt)
{
   const std::vector<int> data = {1, 2, 3};
   double average = stats::mean(data);
   ASSERT_EQ(average, 2.0);
}

TEST(TestStats, TestAverageFloat)
{
   const std::vector<float> data = { 1.0, 2.0, 3.0 };
   double average = stats::mean(data);
   ASSERT_EQ(average, 2.0);
}

TEST(TestStats, TestAverageDouble)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0 };
   double average = stats::mean(data);
   ASSERT_EQ(average, 2.0);
}

TEST(TestStats, TestMedianDoubleEven)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0 };
   double median = stats::median(data);
   ASSERT_EQ(median, 4.5);
}

TEST(TestStats, TestMedianDoubleOdd)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0 };
   double median = stats::median(data);
   ASSERT_EQ(median, 4.0);
}

TEST(TestStats, TestStandardDeviationInt)
{
   const std::vector<int> data = { 1, 2, 3 };
   double dev = stats::standard_deviation(data);
   ASSERT_EQ(dev, 1.0);
}

TEST(TestStats, TestStandardDeviationFloat)
{
   const std::vector<float> data = { 1.0, 2.0, 3.0 };
   double dev = stats::standard_deviation(data);
   ASSERT_EQ(dev, 1.0);
}

TEST(TestStats, TestStandardDeviationDouble)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0 };
   double dev = stats::standard_deviation(data);
   ASSERT_EQ(dev, 1.0);
}

TEST(TestStats, TestRelativeStandardDeviationDouble)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0 };
   double rsd = stats::relative_standard_deviation(data);
   ASSERT_EQ(rsd, 0.5);
}

TEST(TestStats, TestCoefficientOfVariationDouble)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0 };
   double cv = stats::coefficient_of_variation(data);
   ASSERT_EQ(cv, 50.0);
}

TEST(TestStats, TestQuantileDouble)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0 };
   const std::vector<double> quantiles = stats::quantile(data, { 0.25, 0.5, 0.75 });
   ASSERT_EQ(quantiles.size(), 3);
}

TEST(TestStats, TestLinearRegressionSimple)
{
   const std::vector<double> x = { 1.0, 2.0, 3.0 };
   const std::vector<double> y = { 1.0, 2.0, 3.0 };
   stats::linregr result = stats::linear_regression(x, y);
   ASSERT_EQ(result.gradient, 1.0);
   ASSERT_EQ(result.intercept, 0.0);
}

TEST(TestStats, TestLinearRegressionSimple2)
{
   const std::vector<double> x = { 1.0, 2.0, 3.0 };
   const std::vector<double> y = { 1.0, 1.5, 2.0 };
   stats::linregr result = stats::linear_regression(x, y);
   ASSERT_EQ(result.gradient, 0.5);
   ASSERT_EQ(result.intercept, 0.5);
}

TEST(TestStats, TestSimpleMovingAverage)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   const int block_size = 3;
   std::vector<double> average = stats::simple_moving_average(data, block_size);
   ASSERT_EQ(data.size()-(block_size-1), average.size());
   ASSERT_THAT(average, testing::ElementsAre(2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0));
}

TEST(TestStats, TestSimpleMovingAverageCentredOdd)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
   const int block_size = 3;
   std::vector<double> average = stats::simple_moving_average_centred(data, block_size);
   ASSERT_EQ(data.size(), average.size());
   ASSERT_THAT(average, testing::ElementsAre(1.5, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 8.5));
}

TEST(TestStats, TestSimpleMovingAverageCentredEven)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   const int block_size = 4;
   std::vector<double> average = stats::simple_moving_average_centred(data, block_size);
   ASSERT_EQ(data.size(), average.size());
   ASSERT_THAT(average, testing::ElementsAre(1.5, 2, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9));
}

TEST(TestStats, TestCumulativeMovingAverage)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   std::vector<double> average = stats::cumulative_moving_average(data);
   ASSERT_EQ(data.size(), average.size());
   const double double_error = 1e-4;
   const testing::Matcher<double> expected_array[10] = {
      testing::DoubleNear(1.0, double_error), 
      testing::DoubleNear(3.0, double_error), 
      testing::DoubleNear(4.5, double_error), 
      testing::DoubleNear(5.83333, double_error), 
      testing::DoubleNear(7.08333, double_error), 
      testing::DoubleNear(8.28333, double_error), 
      testing::DoubleNear(9.45, double_error),
      testing::DoubleNear(10.5929, double_error), 
      testing::DoubleNear(11.7179, double_error), 
      testing::DoubleNear(12.829, double_error)
   };
   EXPECT_THAT(average, testing::ElementsAreArray(expected_array));
}

TEST(TestStats, TestSimpleMovingStandardDeviation)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   const int block_size = 3;
   std::vector<double> stdev = stats::simple_moving_standard_deviation(data, block_size);
   ASSERT_EQ(data.size() - (block_size - 1), stdev.size());
   ASSERT_THAT(stdev, testing::ElementsAre(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0));
}

TEST(TestStats, TestSimpleMovingStandardDeviationCentredOdd)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   const int block_size = 3;
   std::vector<double> stdev = stats::simple_moving_standard_deviation_centred(data, block_size);
   ASSERT_EQ(data.size(), stdev.size());
   const double double_error = 1e-4;
   const testing::Matcher<double> expected_array[10] = {
      testing::DoubleNear(0.840896, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(0.840896, double_error)
   };
   EXPECT_THAT(stdev, testing::ElementsAreArray(expected_array));
}

TEST(TestStats, TestSimpleMovingStandardDeviationCentredEven)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   const int block_size = 4;
   std::vector<double> stdev = stats::simple_moving_standard_deviation_centred(data, block_size);
   ASSERT_EQ(data.size(), stdev.size());
   const double double_error = 1e-4;
   const testing::Matcher<double> expected_array[10] = {
      testing::DoubleNear(0.840896, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.13622, double_error),
      testing::DoubleNear(1.13622, double_error),
      testing::DoubleNear(1.13622, double_error),
      testing::DoubleNear(1.13622, double_error),
      testing::DoubleNear(1.13622, double_error),
      testing::DoubleNear(1.13622, double_error),
      testing::DoubleNear(1.13622, double_error),
      testing::DoubleNear(1.0, double_error)
   };
   EXPECT_THAT(stdev, testing::ElementsAreArray(expected_array));
}

TEST(TestStats, TestExponetialMovingAverage)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   std::vector<double> average = stats::exponential_moving_average(data);
   ASSERT_EQ(data.size(), average.size());
   const double double_error = 1e-4;
   const testing::Matcher<double> expected_array[10] = {
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(2.0, double_error),
      testing::DoubleNear(2.6666, double_error),
      testing::DoubleNear(3.3333, double_error),
      testing::DoubleNear(4.0, double_error),
      testing::DoubleNear(4.6666, double_error),
      testing::DoubleNear(5.3333, double_error),
      testing::DoubleNear(6.0, double_error),
      testing::DoubleNear(6.6666, double_error),
      testing::DoubleNear(7.3333, double_error)
   };
   EXPECT_THAT(average, testing::ElementsAreArray(expected_array));
}

TEST(TestStats, TestExponetialMovingDeviation)
{
   const std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
   std::vector<double> average = stats::exponential_moving_standard_deviation(data);
   ASSERT_EQ(data.size(), average.size());
   const double double_error = 1e-4;
   const testing::Matcher<double> expected_array[10] = {
      testing::DoubleNear(0.0, double_error),
      testing::DoubleNear(0.0, double_error),
      testing::DoubleNear(0.4714, double_error),
      testing::DoubleNear(0.7453, double_error),
      testing::DoubleNear(1.0, double_error),
      testing::DoubleNear(1.2472, double_error),
      testing::DoubleNear(1.4907, double_error),
      testing::DoubleNear(1.7320, double_error),
      testing::DoubleNear(1.9720, double_error),
      testing::DoubleNear(2.2110, double_error)
   };
   EXPECT_THAT(average, testing::ElementsAreArray(expected_array));
}

TEST(TestStats, TestPercentageIncrease)
{
   EXPECT_THAT(0.5, stats::percentage_increase(10.0, 15.0));
}