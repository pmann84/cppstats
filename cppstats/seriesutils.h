#ifndef __SERIES_UTILS_H__
#define __SERIES_UTILS_H__

namespace stats
{
   namespace seriesutils
   {
      /// Calulates the series sum of a series of data
      /** Calculates the series sum of a series of data
       ** with each element of the series being operated
       ** on by the user defined function f. */
      template<typename T>
      T series_sum(typename std::vector<T>::const_iterator begin, typename std::vector<T>::const_iterator end, std::function<T(T)> f)
      {
         T sum = 0;
         std::for_each(begin, end, [&sum, &f](T element) { sum += f(element); });
         return sum;
      }

      template<typename T>
      T series_sum(typename std::vector<T>::const_iterator begin, typename std::vector<T>::const_iterator end)
      {
         std::function<T(T)> no_op = [](T element) -> T { return element; };
         return seriesutils::series_sum(begin, end, no_op);
      }

      template<typename T>
      T series_sum(const std::vector<T>& data, std::function<T(T)> f)
      {
         return seriesutils::series_sum(data.cbegin(), data.cend(), f);
      }

      template<typename T>
      T series_sum(const std::vector<T>& data)
      {
         std::function<T(T)> no_op = [](T element) -> T { return element; };
         return seriesutils::series_sum(data, no_op);
      }

      //template<typename T>
      //T series_sum(const std::vector<T>&... datas, std::function<T(T...)> f)
      //{
      //   
      //}

      template<typename T>
      std::vector<T> series_product(const std::vector<T>& data1, const std::vector<T>& data2)
      {
         if (data1.size() != data2.size())
         {
            throw;
         }
         std::vector<T> sum;
         sum.reserve(data1.size());
         for (int i = 0; i < data1.size(); ++i)
         {
            sum.push_back(data1[i] * data2[i]);
         }
         return sum;
      }

      template<typename T>
      std::vector<T> series_square(const std::vector<T>& data)
      {
         std::vector<T> sum;
         sum.reserve(data.size());
         std::for_each(data.begin(), data.end(), [&sum](T n) { sum.push_back(n*n); });
         return sum;
      }

      template<typename T>
      std::vector<unsigned int> rank(const std::vector<T>& data)
      {
         std::vector<unsigned int> ranks;
         ranks.reserve(data.size());
         for (int i = 0; i < data.size(); ++i)
         {
            ranks.push_back(i + 1);
         }
         std::vector<unsigned int> ranks_ref(ranks);
         std::sort(ranks.begin(), ranks.end(), [data, ranks_ref](const int a, const int b) {
            int a_idx = std::distance(ranks_ref.begin(), std::find(ranks_ref.begin(), ranks_ref.end(), a));
            int b_idx = std::distance(ranks_ref.begin(), std::find(ranks_ref.begin(), ranks_ref.end(), b));
            return data[a_idx] < data[b_idx];
         });
         return ranks;
      }

      template<typename T>
      std::vector<T> operator+(const std::vector<T>& vector1, const std::vector<T>& vector2)
      {
         std::vector<T> new_vector;
         const int smallest_size = vector1.size() > vector2.size() ? vector2.size() : vector1.size();
         for (int i = 0; i < smallest_size; ++i)
         {
            new_vector.push_back(vector1.at(i) + vector2.at(i));
         }
         return new_vector;
      }
   }
}
#endif // __SERIES_UTILS_H__