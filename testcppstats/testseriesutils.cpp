#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "../cppstats/seriesutils.h"

//double double_rand(double min, double max)
//{
//   double f = static_cast<double>(rand()) / RAND_MAX;
//   return min + f * (max - min);
//}
//
//std::vector<double> double_rand_vector(int size, double min, double max)
//{
//   std::vector<double> rand_vector;
//   rand_vector.reserve(size);
//   for (int i=0; i<size; ++i)
//   {
//      rand_vector.push_back(double_rand(min, max));
//   }
//   return rand_vector;
//}

//TEST(TestSeriesUtils, TestSeriesSumLarge)
//{
//   std::vector<double> data = double_rand_vector(100000, -1.0, 1.0);
//   int sum = stats::seriesutils::series_sum(data);
//   ASSERT_EQ(sum, 6);
//}

TEST(TestSeriesUtils, TestSeriesSum)
{
   std::vector<int> data = { 1, 2, 3 };
   int sum = stats::seriesutils::series_sum(data);
   ASSERT_EQ(sum, 6);
}

TEST(TestSeriesUtils, TestSeriesSumDouble)
{
   std::vector<double> data = { 1.0, 2.0, 3.0, 4.0, 5.0 };
   double sum = stats::seriesutils::series_sum(data);
   EXPECT_THAT(sum, testing::DoubleEq(15.0));
}

TEST(TestSeriesUtils, TestSeriesSumEmpty)
{
   std::vector<double> data = {};
   double sum = stats::seriesutils::series_sum(data);
   EXPECT_THAT(sum, testing::DoubleEq(0.0));
}

TEST(TestSeriesUtils, TestSeriesProduct)
{
   std::vector<int> data1 = { 1, 2, 3 };
   std::vector<int> data2 = { 1, 2, 3 };
   std::vector<int> product = stats::seriesutils::series_product(data1, data2);
   ASSERT_EQ(product.size(), 3);
   ASSERT_EQ(product[0], 1);
   ASSERT_EQ(product[1], 4);
   ASSERT_EQ(product[2], 9);
}

TEST(TestSeriesUtils, TestSeriesSquare)
{
   std::vector<int> data1 = { 1, 2, 3 };
   std::vector<int> square = stats::seriesutils::series_square(data1);
   ASSERT_EQ(square.size(), 3);
   ASSERT_EQ(square[0], 1);
   ASSERT_EQ(square[1], 4);
   ASSERT_EQ(square[2], 9);
}

TEST(TestSeriesUtils, TestSeriesRank)
{
   std::vector<int> data = { 1, 2, 3, 9, 6 };
   std::vector<unsigned int> ranks = stats::seriesutils::rank(data);
   ASSERT_EQ(ranks.size(), data.size());
   ASSERT_THAT(ranks, testing::ElementsAre(1, 2, 3, 5, 4));
}

// TODO: Fix this test
//TEST(TestSeriesUtils, TestSeriesRank2)
//{
//   // { 8, 2, 3, 9, 6, 10, 5, 1 }
//   // { 6, 2, 3, 7, 5,  8, 4, 1 }
//   std::vector<int> data = { 8, 2, 3, 9, 6, 10, 5, 1 };
//   std::vector<unsigned int> ranks = stats::seriesutils::rank(data);
//   ASSERT_EQ(ranks.size(), data.size());
//   ASSERT_THAT(ranks, testing::ElementsAre(6, 2, 3, 7, 5, 8, 4, 1));
//}

TEST(TestSeriesUtils, TestSeriesRankDuplicateValues)
{
   std::vector<int> data = { 8, 2, 3, 9, 6, 10, 5, 2 };
   std::vector<unsigned int> ranks = stats::seriesutils::rank(data);
   ASSERT_EQ(ranks.size(), data.size());
}